# P1 Server

This P1 Server is meant to read the telegrams send by Dutch smart meters. It's purpose is to read the data and make it accessible through a simple HTTP-server.

Please note that the HTTP-server is just a simple Python socket connection - without any of the security features you'll find in a full webserver like Apache. This server is not suitable for connections from outside your network.

This script is tested on a Raspberry Pi 3B and a Raspberry Pi Zero connected to the P1 port of a Kaifa E0003 installed by Stedin in the Rotterdam, NL area. This script is tested on Python version 2.7.5.

## Installation

### Cloning
To install P1 Server you can clone this repository. Execute the following command:

`git clone https://bitbucket.org/elwinb/p1-server.git`

The files are stored in the subfolder `p1-server` on your current path. If you want to clone the files to a specific location use:

`git clone https://bitbucket.org/elwinb/p1-server.git ~/path/to/your/location`

When your Git ask for your Bitbucket credentials you can simply hit Enter or login with your Bitbucket account.

To checkout a specifgic version of the P1 Server use:

`git checkout v1.0.0`

### Executable
The P1 Server is a Python script which should be started with the prefix `python`. You can make the file an executable with the following command:

`sudo chmod +x p1-server.py`

### Python modules
The Python Serial module is needed to run this script. If not yet installed, run the following commands.

Install Python Package Manager (PIP):

`sudo apt install python-pip`

Install the module:

`pip install pyserial`

## Configuration

P1 Server comes with a config file. You should change the variables in your config to suit your needs. The following configuration values are available:

| Section    | Variable | Default        | Accepted values      | Description                                                                                                              |
| ---------- | -------- | -------------- | -------------------- | ------------------------------------------------------------------------------------------------------------------------ |
| general    | debug    | `True`         | `True`, `False`      | Show debug output after starting the P1 Server. Not recommended for production or when the server is started as service. |
| server     | port     | `8082`         | Any integer > `1024` | Port for the webserver, recommended are ports `8080` - `8082` depending on the servers already running.                  |
|            | max      | `5`            | Any integer          | Number of possible concurrent connections to the webserver. Set as low as possible for lower load.                       |
| smartmeter | dsmr     | `4`            | `2`, `4`             | DSMR version of your smart meter:<br>`2`: 2.2<br>`4`: 4.0, 4.2                                                           |
|            | timeout  | `10`           | Any integer > `1`    | Tineout in seconds before connecting with smart meter is terminated.                                                     |
|            | port     | `/dev/ttyUSB0` | N/A                  | USB port where the smart meter is connected                                                                              |

## Run
To run the P1 Server enter the following command in the command line:

`python p1-server.py`

If you changed the file in to an executable, you can use:

`./p1-server.py`

### Run as service
You can also run the P1-server as a service. Follow these steps to run the script as a service:

1. Copy the example file from the source code: `sudo cp p1-server.service.example /lib/systemd/system/p1-server.service`;
2. Update the content of the file to match your environment;
3. Update file permissions: `sudo chmod 644 /lib/systemd/system/p1-server.service`;
4. Now the unit file has been defined you can tell systemd to start it during the boot sequence: `sudo systemctl daemon-reload`;
5. Enable your service on system boots: `sudo systemctl enable p1-server.service`;
6. Reboot to test the service.

# Credits
Thanks to Franklin for the reasearch for running the script as a service and for testing.
