#!/usr/bin/python

from __future__ import print_function
from threading import Thread
import datetime
import re
import serial
import time
import socket
import json
import ConfigParser
import sys

class State(object):

    def __init__(self):
        # Read the config file
        self.config = ConfigParser.ConfigParser()
        self.config.read(sys.path[0] + '/config.ini')

        # Do we need debug output
        self.debug = self.config.getboolean('general', 'debug')

        # Set default vvariables
        self.datetime = datetime.datetime.utcnow()
        self.consumption_t1 = 00000.000
        self.consumption_t2 = 00000.000
        self.returned_t1 = 00000.000
        self.returned_t2 = 00000.000
        self.power_consumption = 0
        self.power_returned = 0
        self.gas_total = 00000.000
        self.gas_datetime = datetime.datetime.fromtimestamp(0)
        self.rate = 0

        # Start new thread for the webserver
        self.consumer_thread = Thread(target=self.server)
        self.consumer_thread.daemon = True
        self.consumer_thread.start()

    def readUsb(self):
        # Serail port configuration
        ser = serial.Serial()

        # DSMR version from the config
        dsmr = self.config.get('smartmeter', 'dsmr');
        if self.debug:
            print('DSMR version:',dsmr);

        if dsmr == 2:
            # DSMR 2.2 > 9600 7E1:
            ser.baudrate = 9600
            ser.bytesize = serial.SEVENBITS
            ser.parity = serial.PARITY_EVEN
            ser.stopbits = serial.STOPBITS_ONE
        else:
            # DSMR 4.0/4.2 > 115200 8N1:
            ser.baudrate = 115200
            ser.bytesize = serial.EIGHTBITS
            ser.parity = serial.PARITY_NONE
            ser.stopbits = serial.STOPBITS_ONE

        ser.xonxoff = 0
        ser.rtscts = 0
        ser.timeout = int(self.config.get('smartmeter', 'timeout'))
        ser.port = self.config.get('smartmeter', 'port')
        ser.close()

        while True:
            try:
                if self.debug:
                    print('Trying to connect to smartmeter')

                ser.open()
                checksum_found = False

                if ser.isOpen():
                    if self.debug:
                        print('Connected to smartmeter')

                    while not checksum_found:
                        # Read the line of the output of the serial connection
                        telegram_line = ser.readline()

                        # Strip data of empty lines and spaces
                        telegram_line = telegram_line.decode('ascii').strip()

                        # Total consumption T1
                        if re.match(b'(?=1-0:1.8.1)', telegram_line):
                            # New value
                            value = float(telegram_line[10:20]);

                            # Debug
                            if self.debug:
                                print('Total consumption T1')
                                print('- Current: ',self.consumption_t1)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.consumption_t1 != value:
                                self.consumption_t1 = value;

                        # Total consumption T2
                        elif re.match(b'(?=1-0:1.8.2)', telegram_line):
                            # New value
                            value = float(telegram_line[10:20]);

                            # Debug
                            if self.debug:
                                print('Total consumption T2')
                                print('- Current: ',self.consumption_t2)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.consumption_t2 != value:
                                self.consumption_t2 = value;

                        # Total returned T1
                        elif re.match(b'(?=1-0:2.8.1)', telegram_line):
                            # New value
                            value = float(telegram_line[10:20]);

                            # Debug
                            if self.debug:
                                print('Total returned T1')
                                print('- Current: ',self.returned_t1)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.returned_t1 != value:
                                self.returned_t1 = value;

                        # Total returned T2
                        elif re.match(b'(?=1-0:2.8.2)', telegram_line):
                            # New value
                            value = float(telegram_line[10:20]);

                            # Debug
                            if self.debug:
                                print('Total returned T2')
                                print('- Current: ',self.returned_t2)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.returned_t2 != value:
                                self.returned_t2 = value;

                        # Current power consumption
                        elif re.match(b'(?=1-0:1.7.0)', telegram_line):
                            # New value
                            value = int(float(telegram_line[10:16]) * 1000)

                            # Debug
                            if self.debug:
                                print('Current power consumption')
                                print('- Current: ',self.power_consumption)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.power_consumption != value:
                                self.power_consumption = value;

                        # Current power returned
                        elif re.match(b'(?=1-0:2.7.0)', telegram_line):
                            # New value
                            value = int(float(telegram_line[10:16]) * 1000)

                            # Debug
                            if self.debug:
                                print('Current power returned')
                                print('- Current: ',self.power_returned)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.power_returned != value:
                                self.power_returned = value;

                        # Gas (usage and time)
                        elif re.match(b'(?=0-1:24.2.1)', telegram_line):
                            # Usage

                            # New value
                            value = float(telegram_line[26:35])

                            # Debug
                            if self.debug:
                                print('Total gas')
                                print('- Current: ',self.gas_total)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.gas_total != value:
                                self.gas_total = value;

                            # Time

                            # New value
                            value = telegram_line[11:24]
                            datetime_obj = datetime.datetime.strptime(value[0:-1], '%y%m%d%H%M%S')

                            # Debug
                            if self.debug:
                                print('Gas reading time')
                                print('- Current: ',self.gas_datetime.strftime("%Y-%m-%d %H:%M:%S"))

                            if value[-1] == 'W':
                                # Winter time, UTC +1
                                offset = datetime.timedelta(hours=1)
                                datetime_obj_utc = datetime_obj - offset

                                if self.gas_datetime != datetime_obj_utc:
                                    self.gas_datetime = datetime_obj_utc

                            else:
                                # Summer time, UTC +2
                                offset = datetime.timedelta(hours=2)
                                datetime_obj_utc = datetime_obj - offset

                                if self.gas_datetime != datetime_obj_utc:
                                    self.gas_datetime = datetime_obj_utc

                            # Debug
                            if self.debug:
                                print('- New: ',datetime_obj_utc.strftime("%Y-%m-%d %H:%M:%S"))
                                print()

                        # Rate
                        elif re.match(b'(?=0-0:96.14.0)', telegram_line):
                            # New value
                            value = int(telegram_line[12:16])

                            # Debug
                            if self.debug:
                                print('Rate')
                                print('- Current: ',self.rate)
                                print('- New: ',value)
                                print()

                            # Save new value
                            if self.rate != value:
                                self.rate = value;

                        # When the checksum is found, set the flag to true
                        if re.match(b'(?=!)', telegram_line):
                            checksum_found = True

                            # Save datetime
                            self.datetime = datetime.datetime.utcnow()

                # Close connection for this loop
                ser.close()

            except Exception as ex:
                if self.debug:
                    print('------------------------------------------------------------')
                    print('Exception:')
                    print(ex)
                    print('------------------------------------------------------------')

                checksum_found = False
                ser.close()
                time.sleep(5)

    def notFound(self):
        header = 'HTTP/1.1 404 Not Found\r\n'
        header += 'Content-Type: application/json\r\n\r\n'

        data = {
            'status': 404,
            'message': 'Not Found'
        }

        return header, data;

    def notAllowed(self):
        header = 'HTTP/1.1 405 Method Not Allowed\r\n'
        header += 'Content-Type: application/json\r\n\r\n'

        data = {
            'status': 405,
            'message': 'Method Not Allowed'
        }

        return header, data;

    def getData(self):
        header = 'HTTP/1.1 200 OK\r\n'
        header += 'Content-Type: application/json\r\n\r\n'

        data = {
            'datetime': self.datetime.strftime('%Y-%m-%d %H:%M:%S+00:00'),
            'consumption_t1': self.consumption_t1,
            'consumption_t2': self.consumption_t2,
            'returned_t1': self.returned_t1,
            'returned_t2': self.returned_t2,
            'power_consumption': self.power_consumption,
            'power_returned': self.power_returned,
            'gas_total': self.gas_total,
            'gas_datetime': self.gas_datetime.strftime('%Y-%m-%d %H:%M:%S+00:00'),
            'rate': self.rate
        }

        return header, data;

    def server(self):
        # Get port from config file
        port = int(self.config.get('server', 'port'))

        # Max connections from config file
        max = int(self.config.get('server', 'max'))

        # Open socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('', port))
        sock.listen(max)

        if self.debug:
            print('Serving on port:',port)
            print('Maximum concurrent connections:',max)

        while True:
            connection,address = sock.accept()
            request = connection.recv(4096).decode('utf-8')

            # Split request from spaces
            string_list = request.split(' ')

            # Get the method
            method = string_list[0]

            # Check request method
            if method != 'GET':
                # Error, method not allowed

                # Log error
                if self.debug:
                    print('Method not allowed: ',method)

                # Get header and data for the response
                header, data = self.notAllowed()

            elif len(string_list) == 1 or string_list[1] != '/':
                # Error, request not found

                # Log error
                if self.debug:
                    print('Not found: ',string_list)

                # Get header and data for the response
                header, data = self.notFound()

            else:
                # GET request, respond with the data

                # Log request
                if self.debug:
                    print('Data requested')

                # Get header and data for the response
                header, data = self.getData()

            # Final respnse: header and data
            final = header.encode('utf-8')
            final += json.dumps(data)

            # Send final response
            connection.send(final)

            # Close connection
            connection.close()

if __name__ == "__main__":
    state = State()
    state.readUsb()
